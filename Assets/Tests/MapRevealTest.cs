﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapReveal.Tests
{
    public class MapRevealTest : BaseMapRevealManager
    {
        private void Start()
        {

        }

        protected override void Update()
        {
            base.Update();
            ApplyPixels();

            if (Input.GetKeyDown(KeyCode.Space))
                SaveToFile();
        }
    }
}