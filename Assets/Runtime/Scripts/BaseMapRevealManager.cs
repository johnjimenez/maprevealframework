﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace MapReveal
{
    public class BaseMapRevealManager : MonoBehaviour
    {
        public Texture2D texture2D;
        public Transform revealPos;
        public int resolution = 256;
        public float radius = 20;
        public float pixelsPerUnit = 5;

        private Vector3 lastPos;

        public List<MapArea> MapAreas;
        public MapArea CurrentMapArea;

        public Bounds RevealerBounds;

        private void Awake()
        {
            MapAreas = GetComponentsInChildren<MapArea>().ToList();
            RevealerBounds = new Bounds(revealPos.position, radius * Vector3.one);
        }

        public void LoadArea(MapArea area)
        {
            if (CurrentMapArea != null)
                UnloadMapArea();

            print($"loading: {area.AreaName}");

            CurrentMapArea = area;

            if (!area.HasRevealData)
            {
                texture2D.Resize(1, 1);
                Clear(Color.white);
            }
            else
            {
                if (File.Exists(GetFilename(area)))
                {
                    var data = File.ReadAllBytes(GetFilename(area));
                    texture2D.LoadImage(data);
                }
                else
                {
                    texture2D.Resize(resolution, resolution);
                    Clear(Color.black);

                    SaveToFile();
                }
            }
        }

        public void UnloadMapArea()
        {
            if (!CurrentMapArea.HasRevealData)
                return;

            print($"unloading: {CurrentMapArea.AreaName}");

            SaveToFile();
            CurrentMapArea = null;
        }

        protected virtual void Update()
        {
            var d = (revealPos.position - lastPos);
            if (d.sqrMagnitude < pixelsPerUnit * pixelsPerUnit)
                return;

            lastPos = revealPos.position;
            RevealerBounds.center = revealPos.position;

            foreach (var i in MapAreas)
            {
                if (!i.HasRevealData)
                    continue;

                if (i.AreaBounds.Contains(lastPos))
                {
                    if (i.Contains(lastPos))
                    {
                        if (CurrentMapArea != i)
                        {
                            LoadArea(i);
                            break;
                        }
                    }
                }
                else
                {
                    if (CurrentMapArea == i)
                    {
                        UnloadMapArea();
                    }
                }
            }

            if (CurrentMapArea == null)
                return;

            var pos = CurrentMapArea.GetUVPosition(revealPos.position);
            DrawCircle(Remap(pos), radius);
        }

        public void ApplyPixels()
        {
            texture2D.Apply();
        }

        public void SaveToFile()
        {
            texture2D.Apply();
            var bytes = texture2D.EncodeToPNG();
            File.WriteAllBytes(GetFilename(CurrentMapArea), bytes);
        }

        private void Clear(Color color)
        {
            for (int x = 0; x < texture2D.width; x++)
            {
                for (int y = 0; y < texture2D.height; y++)
                {
                    texture2D.SetPixel(x, y, color);
                }
            }

            ApplyPixels();
        }

        private void DrawCircle(Vector2 pos, float r)
        {
            var color = Color.red;
            float rSquared = r * r;

            int x = Mathf.RoundToInt(pos.x);
            int y = Mathf.RoundToInt(pos.y);

            for (int u = 0; u < texture2D.width; u++)
            {
                for (int v = 0; v < texture2D.height; v++)
                {
                    var d = (x - u) * (x - u) + (y - v) * (y - v);
                    if (d < rSquared)
                    {
                        var px = texture2D.GetPixel(u, v);

                        color.r = Mathf.Lerp(1f, 0f, (float)d / (float)rSquared);
                        texture2D.SetPixel(u, v, px + color);
                    }
                }
            }
        }

        private Vector3 Remap(Vector3 pos)
        {
            var halfSize = CurrentMapArea.AreaSize / 2;
            var mapPos = CurrentMapArea.Position;

            return new Vector3()
            {
                x = Remap(pos.x, -halfSize, halfSize, 0, texture2D.width),
                y = Remap(pos.y, -halfSize, halfSize, 0, texture2D.height),
            };
        }

        private float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        protected virtual string GetFilename(MapArea area)
        {
            return $"F:/Map-{area.AreaName}.png";
        }
    }
}