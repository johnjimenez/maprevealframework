﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapReveal
{
    public class MapArea : MonoBehaviour
    {
        public string AreaName;
        public Bounds AreaBounds;
        public Vector2 Position => transform.position;
        public bool HasRevealData;
        public int AreaSize = 2048;

        public bool[,] Pixels;
        public bool IsActive;

        [SerializeField] private PolygonCollider2D polygonCollider = null;

        private void Awake()
        {
            AreaBounds = new Bounds(Position, AreaSize * Vector3.one);
        }

        public bool Contains(Vector2 point)
        {
            if (polygonCollider == null)
                return false;

            return polygonCollider.OverlapPoint(point);
        }

        public Vector3 GetUVPosition(Vector3 playerPos)
        {
            var pos = transform.InverseTransformPoint(playerPos);
            return pos;
        }


        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(Position, new Vector3(AreaSize, AreaSize, 1));
        }
    }
}